import bisect
from hashlib import md5

class ConsistentHashRing(object):
    """Implement a consistent hashing ring."""

    def __init__(self, replicas=None):
        """Create a new ConsistentHashRing.

        :param replicas: number of replicas.

        """
        if replicas is None:
            self.replicas = 1
        else:
            self.replicas = replicas
        self._keys = []
        self._nodes = {}

    def _hash(self, key):
        """Given a string key, return a hash value."""

        return long(md5(key).hexdigest(), 16)

    def _repl_iterator(self, nodename):
        """Given a node name, return an iterable of replica hashes."""

        return (self._hash("%s:%s" % (nodename, i))
                for i in xrange(self.replicas))

    def __setitem__(self, nodename, node):
        """Add a node, given its name.

        The given nodename is hashed
        among the number of replicas.

        """
        for hash_ in self._repl_iterator(nodename):
            if hash_ in self._nodes:
                raise ValueError("Node name %r is "
                            "already present" % nodename)
            self._nodes[hash_] = node
            bisect.insort(self._keys, hash_)

    def __delitem__(self, nodename):
        """Remove a node, given its name."""

        for hash_ in self._repl_iterator(nodename):
            # will raise KeyError for nonexistent node name
            del self._nodes[hash_]
            index = bisect.bisect_left(self._keys, hash_)
            del self._keys[index]

    def __getitem__(self, key):
        """Return a node, given a key.

        The node replica with a hash value nearest
        but not less than that of the given
        name is returned.   If the hash of the
        given name is greater than the greatest
        hash, returns the lowest hashed node.

        """
        hash_ = self._hash(key)
        start = bisect.bisect(self._keys, hash_)
        if start == len(self._keys):
            start = 0
        return self._nodes[self._keys[start]]

import requests
import json
data_set = json.load(open("data_set.json",'r'))

cr = ConsistentHashRing()
cr['node1'] = '127.0.0.1:3000'
cr['node2'] = '127.0.0.1:4000'
cr['node3'] = '127.0.0.1:5000'

print "Posting data and looping through Id -->\n"
for data_ in data_set:
    headers = {'content-type':'application/json'}
    id_ = data_['id']
    host = cr[id_]
    print "\""+id_+"\""+"=>"+host
    r = requests.post(url = 'http://'+host+'/v1/expenses',data=json.dumps(data_))


print "\n\nGetting the data by GET from DB -->\n"
for id_ in range(len(data_set)):
    host = cr[str(id_+1)]
    print "\""+str(id_)+"\""+"=>"+host
    r = requests.get(url = 'http://'+host+'/v1/expenses/'+str(id_+1))
    print r.content

