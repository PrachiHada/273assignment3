from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import config as cf

app = Flask(__name__)
DATABASE = cf.DATABASE_DB
#DATABASE = 'Expenses'
PASSWORD = cf.DATABASE_PASSWORD
#PASSWORD = 'aichomavao'
USER = cf.DATABSE_USER
#USER = 'root'
HOSTNAME = cf.DATABASE_HOST
#HOSTNAME = 'db:3306'

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://%s:%s@%s/%s' %(USER,PASSWORD,HOSTNAME,DATABASE)
db = SQLAlchemy(app)



class Expenses(db.Model):
    id = db.Column(db.Integer,primary_key = True)
    name = db.Column(db.String(50))
    email = db.Column(db.String(100))
    category = db.Column(db.String(100))
    description = db.Column(db.String(100))
    link = db.Column(db.String(100))
    estimated_costs = db.Column(db.Integer)
    submit_date = db.Column(db.String(100))
    status = db.Column(db.String(100))
    decision_date = db.Column(db.String(100))

    def __init__(self,id,name,email,category,description,link,estimated_costs,submit_date,status,decision_date):
        self.id = id
        self.name = name
        self.email = email
        self.category = category
        self.description = description
        self.link = link
        self.estimated_costs = estimated_costs
        self.submit_date = submit_date
        self.status = status
        self.decision_date = decision_date

    def __repr__(self):
        return '<Expense %r>' %(self.id)

    def __str__(self):
        return '{\n"id" : "%s",\n"name" : "%s",\n"email" : "%s",\n"category" : "%s",\
                \n"description" : "%s",\n"link" : "%s",\n"estimated_costs" : "%s",\
                \n"submit_date" : "%s",\n"status" : "%s",\n"decision_date" : "%s"\n}' \
                %(self.id,self.name,self.email,self.category,self.description,self.link,self.estimated_costs,self.submit_date,self.status,self.decision_date)

class CreateDB():
	def __init__(self):
		import sqlalchemy
		engine = sqlalchemy.create_engine('mysql+pymysql://%s:%s@%s'%(USER, PASSWORD, HOSTNAME)) # connect to server
		engine.execute("CREATE DATABASE IF NOT EXISTS %s" %(DATABASE)) #create db

class DropDB():
	def __init__(self):
		import sqlalchemy
		engine = sqlalchemy.create_engine('mysql+pymysql://%s:%s@%s'%(USER, PASSWORD, HOSTNAME)) # connect to server
		engine.execute("DROP DATABASE IF EXISTS %s"%(DATABASE)) #drop db